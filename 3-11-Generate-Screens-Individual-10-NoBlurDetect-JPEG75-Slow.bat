@echo off

set EXEPATH=tools\mtn-thumbnail-me-30-2013\mtn.exe
set INPUT=wip
set OUTDIR=Scr

set OUTFILE=.jpg

rem ====================================================================
set ARG1=-P -n -g 0 -L 4:1 -k 000000 -f consolab.ttf -F FFFFFF:14:consolab.ttf:FFFF00:000000:12
set ARG2=-j 75
set ARG3=-b 0.80
set ARG4=-D 0
set ARG5=-c 1
set ARG6=-r 10
set ARG7=-w 0
set ARG8=-h 0
set ARG9=
set ARG10=-Z
set ARG11=-I
set ARG12=
set ARG13=
set ARG14=
set ARG15=
rem ====================================================================

for %%1 in (wip\*.*) do (
%EXEPATH% -o "%OUTFILE%" -O "%OUTDIR%" %ARG1% %ARG2% %ARG3% %ARG4% %ARG5% %ARG6% %ARG7% %ARG8% %ARG9% %ARG10% %ARG11% %ARG12% %ARG13% %ARG14% %ARG15% -T "%%~nx1" "%%~f1"
del /f /q %OUTDIR%\"%%~n1".jpg
)