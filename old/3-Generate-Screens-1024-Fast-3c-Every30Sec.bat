@echo off

set OUTFILE=.MTNScr1024Fast3C30Sec.jpg
set OUTDIR=Scr
set INPUT=wip
set ARG1=-P -n -g 0 -L 4:1 -k 000000 -f consolab.ttf -F FFFFFF:14:consolab.ttf:FFFF00:000000:12
set ARG2=-j 75
set ARG3=-b 0.80
set ARG4=-D 8
set ARG5=-c 3
set ARG6=
set ARG7=-w 1024
set ARG8=-h 0
set ARG9=-s 30
set ARG10=

tools\mtn-thumbnail-me-30-2013\mtn.exe -o "%OUTFILE%" -O "%OUTDIR%" %ARG1% %ARG2% %ARG3% %ARG4% %ARG5% %ARG6% %ARG7% %ARG8% %ARG9% %ARG10% %INPUT%