@echo off

set debugyesno=no
rem set debugyesno=yes

rem Set the current directory to the location of the batch script, using the %0 parameter
pushd "%~dp0"

rem ====================================================================

set input=temp.html
set echonewline=echo. && echo.

rem ====================================================================

set batwget=tools\wget-download-given-url.bat
echo. & echo DEBUG: calling %batwget% %1 %input% & %echonewline%
call  %batwget% %1 %input%
call :pauseifdebugyes

rem ====================================================================

echo. & echo DEBUG: changing code page to 65001/utf-8 (unicode) & %echonewline%
chcp 65001

rem ====================================================================

echo. & echo DEBUG: testing jrepl.bat & %echonewline%
set jrepl=call tools\jrepl.bat
%jrepl%
call :pauseifdebugyes

rem ====================================================================

rem jrepl flags
rem /jmatch put each match on a new line and discards everything that does not match
rem /m multiline
rem /f input
rem /o output
rem "/o -" replace in-place (write to the input file)

echo. & echo DEBUG: title & %echonewline%
%jrepl% "<h1 class='watchpage-title'>\n\s(.*)\s<\/h1>" "$1" /m /jmatch /f %input% /o tmptitle.txt
call :pauseifdebugyes

echo. & echo DEBUG: workaround for wget downloading the page with "&amp;" instead of "&", replacing "&amp;" with "&" & %echonewline%
%jrepl% "&amp;" "&" /f tmptitle.txt /m /o tmptitle2.txt
call :pauseifdebugyes

rem http://stackoverflow.com/questions/6251463/regex-capitalize-first-letter-every-word-also-after-a-special-character-like-a
rem http://www.dostips.com/forum/viewtopic.php?p=39356#p39356
echo. & echo DEBUG: title: capitalize first letter of each word & %echonewline%
%jrepl% "\b([a-z])" "$0.toUpperCase()" /j /f tmptitle2.txt /m /o -
call :pauseifdebugyes

rem ====================================================================

echo. & echo DEBUG: date & %echonewline%
%jrepl% "uploadDate...(.{10})" "$1" /m /jmatch /f %input% /o tmpdate.txt
call :pauseifdebugyes

rem ====================================================================

echo. & echo DEBUG: length & %echonewline%
%jrepl% "<i aria-hidden class='fa fa-clock-o'>\n.*<\/i>\n\s(.{9})" "$1" /m /jmatch /f %input% /o tmplength.txt
call :pauseifdebugyes

rem ====================================================================

rem jrepl.bat does not do lookBEHIND, so using lookAHEAD first & extracting 2 preceding lines (STEP 1) then getting link titles (STEP 2)
rem STEP 1
echo. & echo DEBUG: talents, step 1 & %echonewline%
%jrepl% ".*\n.*\n.*(?=user--has-purchase.row)" "$0" /m /jmatch /f %input% /o tmptalent1.txt
call :pauseifdebugyes

rem STEP 2
echo. & echo DEBUG: talents, step 2 & %echonewline%
%jrepl% "<a href=.*?>(.*?)<\/a>" "$1" /m /jmatch /f tmptalent1.txt /o tmptalent2.txt
call :pauseifdebugyes

rem http://stackoverflow.com/questions/406230/regular-expression-to-match-line-that-doesnt-contain-a-word
echo. & echo DEBUG: deleting "[Forum thread]" & %echonewline%
%jrepl% "^((?!\[Forum thread\]).)*$" "$0" /jmatch /f tmptalent2.txt /o tmptalent3.txt
call :pauseifdebugyes

echo. & echo DEBUG: replacing newlines with ", " & %echonewline%
%jrepl% "\r\n" ", " /f tmptalent3.txt /m /o tmptalent4.txt
call :pauseifdebugyes

echo. & echo DEBUG: talents: capitalize first letter of each word & %echonewline%
%jrepl% "\b([a-z])" "$0.toUpperCase()" /j /f tmptalent4.txt /m /o -
call :pauseifdebugyes

rem ====================================================================

rem STEP 1
echo. & echo DEBUG: genre, step 1 & %echonewline%
%jrepl% ".*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*\n.*(?=..<div class='scene-description__social)" "$0" /m /jmatch /f %input% /o tmpgenre1.txt
call :pauseifdebugyes

rem STEP 2
echo. & echo DEBUG: genre, step 2 & %echonewline%
%jrepl% "<a href=.*?>(.*?)<\/a>" "$1" /m /jmatch /f tmpgenre1.txt /o tmpgenre2.txt
call :pauseifdebugyes

rem for debugging genres
sort <tmpgenre2.txt >tmpgenre5debugoriginalsorted.txt

rem ####################################################################
rem tmpgenre3
rem ####################################################################

set batfixgenres=LP-INTERNAL-FIX-LP-STUPIDITY.BAT
echo. & echo DEBUG: calling %batfixgenres% & %echonewline%
call %batfixgenres%

echo. & echo DEBUG: genres: capitalize first letter of each word & %echonewline%
%jrepl% "\b([a-z])" "$0.toUpperCase()" /j /f tmpgenre3.txt /m /o -
call :pauseifdebugyes

rem ####################################################################
rem tmpgenre4
rem ####################################################################

rem http://www.dostips.com/forum/viewtopic.php?p=38097#p38097
echo. & echo DEBUG: delete duplicate lines with jprel.bat! & %echonewline%
%jrepl% "" "" /N 10 /f "tmpgenre3.txt" ^
    | %SystemRoot%\system32\sort /+11 ^
    | %jrepl% ".*?:(.*)$"  "x=p;p=$1;($1==x?false:$src);" /jmatch /jbeg "var p='',x" ^
    | %SystemRoot%\system32\sort ^
    | %jrepl% "^.*?:" "" > "tmpgenre4.txt"
call :pauseifdebugyes

echo. & echo DEBUG: genre: sorting lines alphabetically & %echonewline%
sort <tmpgenre4.txt >tmpgenre5.txt

echo. & echo DEBUG: genre: replacing newlines with ", " & %echonewline%
%jrepl% "\r\n" ", " /f tmpgenre5.txt /m /o tmpgenre6.txt
call :pauseifdebugyes

rem ====================================================================

echo. & echo DEBUG: setting variables & %echonewline%

set /p postdate=<tmpdate.txt
set /p postgenre=<tmpgenre6.txt
set /p postlength=<tmplength.txt
set /p posttalent=<tmptalent4.txt
set /p posttitle=<tmptitle2.txt

echo. & echo DEBUG: echo variables: & %echonewline%
echo date:"%postdate%"
echo genre:"%postgenre%"
echo length:"%postlength%"
echo talents:"%posttalent%"
echo title:"%posttitle%"
echo title:!posttitle!
%echonewline%
call :pauseifdebugyes
rem pause

rem ====================================================================

set postsite=LegalPorno.com
set out=post.txt
set advert=Made with 14MinutePostGenerator!

echo. & echo DEBUG: writing to %out% & %echonewline%

rem need enableDelayedExpansion and "!" around posttitle because it can include "&"s
setlocal enableDelayedExpansion

echo. & echo DEBUG: writing heading
echo [%postsite%] %posttalent:~0,-2% (!posttitle!) [%postdate%, %postgenre:~0,-2%] >>%out%
%echonewline% >>%out%
echo [b]Выпущено:[/b] %postsite% >>%out%
%echonewline% >>%out%
echo. & echo DEBUG: writing title & %echonewline%
echo [b]Название:[/b] !posttitle! >>%out%
%echonewline% >>%out%
echo [b]В ролях:[/b] %posttalent:~0,-2% >>%out%
%echonewline% >>%out%
echo [b]Год производства:[/b] %postdate% >>%out%
%echonewline% >>%out%
echo [b]Жанр:[/b] %postgenre:~0,-2% >>%out%
%echonewline% >>%out%
echo [b]Продолжительность:[/b] %postlength% >>%out%
%echonewline% >>%out%
echo [b]Информация о видео (MediaInfo):[/b] >>%out%
%echonewline% >>%out%

call :pauseifdebugyes

rem ====================================================================

set batmediainfo=2-Generate-MediaInfo-IMG-Tag-Pre-Tag-Flat.bat
echo. & echo DEBUG: calling %batmediainfo% & %echonewline%
call %batmediainfo%
call :pauseifdebugyes


if exist (MediaInfo.txt) (
	echo. & echo DEBUG: writing MediaInfo.txt to %out% & %echonewline%
	type MediaInfo.txt >>%out%
	)

rem ====================================================================

echo. & echo DEBUG: done & %echonewline%

rem ====================================================================

%echonewline% >>%out%
echo %advert% >>%out%

timeout 10

rem ====================================================================

:pauseifdebugyes
if ["%debugyesno%"]==["yes"] (
	pause
)

rem ====================================================================